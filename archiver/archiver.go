package archiver

import (
	"os"
)

type Archiver interface {
	Archive(archivePath string, filePath string, fileInfo os.FileInfo) error
	Close() error
}
