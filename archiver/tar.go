package archiver

import (
	"archive/tar"
	"io"
	"os"
	"log"
)

const lp = "[mleko.planb.tar] "

type TarArchiver struct {
	writer *tar.Writer
}

func NewTarArchiver(wr io.Writer) *TarArchiver {
	return &TarArchiver{
		writer: tar.NewWriter(wr),
	}
}

func (ar *TarArchiver) Close() error {
	if err := ar.writer.Flush(); nil != err {
		return err
	}
	return ar.writer.Close()
}

func (ar *TarArchiver) Archive(ap string, fp string, fi os.FileInfo) error {
	if (!fi.IsDir()) {
		log.Printf(lp + "debug: write file: %s", fp)
		return ar.writeFile(ap, fp, fi);
	} else {
		return ar.writeDir(ap, fp, fi);
	}
	return nil;
}

func (ar *TarArchiver) writeFile(ap string, fp string, fi os.FileInfo) error {
	f, err := os.OpenFile(fp, os.O_RDONLY, 0755)
	if (nil != err) {
		return err;
	}
	defer f.Close()

	if err := ar.writer.WriteHeader(&tar.Header{
		Name: ap,
		Mode: int64(fi.Mode() & (os.ModePerm )),
		Size: fi.Size(),
		ModTime: fi.ModTime(),

	}); err != nil {
		return err;
	}

	_, err = io.Copy(ar.writer, f)
	if (nil != err) {
		return err;
	}

	return nil;
}

func (ar *TarArchiver) writeDir(ap string, fp string, fi os.FileInfo) error {
	if err := ar.writer.WriteHeader(&tar.Header{
		Name: ap,
		Mode: int64(fi.Mode() & (os.ModePerm )),
		ModTime: fi.ModTime(),

	}); err != nil {
		return err
	}
	return nil
}
