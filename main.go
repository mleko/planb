package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/mleko/planb/archiver"
	"github.com/mleko/arlog"
	"github.com/mleko/arlog/filter"
	flag "github.com/ogier/pflag"
)

const lp = "[mleko.planb] "

type config struct {
	Jobs []job
}

type job struct {
	Src string
	Dst string
}

var verboseMode bool
var debugMode bool

func init() {
	flag.BoolVarP(&verboseMode, "verbose", "v", false, "enable verbose mode")
	flag.BoolVarP(&debugMode, "debug", "d", false, "enable debug mode")
}

func main() {
	flag.Parse()
	alog := arlog.Register()
	logLevel := arlog.INFO
	if (debugMode) {
		logLevel = arlog.DEBUG
	} else if (verboseMode) {
		logLevel = arlog.VERBOSE
	}
	log.Print(logLevel)
	alog.Filters = append(alog.Filters, filter.NewLevelFilter(logLevel))

	configPath := "config.json"
	if flag.NArg() == 2 {
		configPath = flag.Arg(0)
	}

	log.Printf(lp + "Using config file: %s", configPath)

	cfg, err := readConfig(configPath)
	if nil != err {
		log.Print(err.Error())
		os.Exit(2)
	}

	for _, cfg := range cfg.Jobs {
		log.Printf(lp + "Archive %s to %s\n", cfg.Src, cfg.Dst)
		archive(cfg.Src, cfg.Dst)
	}
}

func readConfig(path string) (config, error) {
	var cfg config
	cfgFile, err := os.Open(path)
	if nil != err {
		return cfg, errors.New(fmt.Sprintf("Failed to open config file: %s, %s\n", path, err.Error()))
	}
	cfgBytes, err := ioutil.ReadAll(cfgFile)
	if nil != err {
		return cfg, errors.New(fmt.Sprintf("Failed to read config file: %s %s", path, err.Error()))
	}

	err = json.Unmarshal(cfgBytes, &cfg)
	if nil != err {
		return cfg, errors.New(fmt.Sprintf("Failed to parse config file: %s %s", path, err.Error()))
	}

	return cfg, nil
}

func archive(src string, dst string) {
	file, err := os.OpenFile(dst, os.O_WRONLY | os.O_CREATE, 0755)
	if nil != err {
		panic(err)
	}
	defer file.Close()

	ar := archiver.NewTarArchiver(file)
	defer ar.Close()

	walk(src, ar)
}

func walk(path string, ar archiver.Archiver) error {
	return filepath.Walk(path, makeWalkFnc(ar, path))
}

func makeWalkFnc(ar archiver.Archiver, bp string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		arPath, err := filepath.Rel(bp, path)
		if nil != err {
			panic(err)
		}
		log.Printf(lp + "verbose: process %s", path)
		if arPath == "." {
			log.Print(lp + "verbose: dotdir, ignore")
			return nil
		}
		err = ar.Archive(arPath, path, info)
		if nil != err {
			panic(err)
		}
		return nil
	}
}
